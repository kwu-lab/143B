from sys import *
import sys
import copy
import queue

RL = [queue.Queue(), queue.Queue(), queue.Queue()]
BL = 0
pcbList = []
currProcess = 0

class rcb():
	def __init__(self, res):
		self.free = int(res)
		self.rid = res
		self.wait = queue.Queue()
rcbList = [0, rcb(1), rcb(2), rcb(3), rcb(4)]

				
class pcb():
	def __init__(self, pid, priority):
		self.pid = pid
		self.priority = priority
		self.statusType = 'ready' 
		self.statusList = RL
		self.parent = -1 
		self.older = -1
		
		self.rcb = [[0,-1],[1,0],[2,0],[3,0],[4,0]]
		self.child = -1
		self.younger = -1

		self.pcbIndex = len(pcbList)
		self.toDelete = -1

#############HELPER FUNCTIONS#############
def cleanRL():
	#go through entire list and remove all pid 0 guys
	global RL
	global pcbList
	tempRL = [queue.Queue(), queue.Queue(), queue.Queue()]
	for i in range(3):
		while not RL[i].empty():
			temp = RL[i].get()
			if not temp.pid == 0:
				tempRL[i].put(temp)
			else:
				for r in range(1,5):
					if not pcbList[temp.pcbIndex].rcb[r][1] <= 0:
						release(r, pcbList[temp.pcbIndex].rcb[r][1])
	RL = tempRL

def scheduler():
	global currProcess
	global RL

	highProcess = 0
	cleanRL()

	#create deep copy of RL so get() does not modify it
	tempRL = [queue.Queue(), queue.Queue(), queue.Queue()]
	tempRLlate = [queue.Queue(), queue.Queue(), queue.Queue()]
	for i in range(3):
		while not RL[i].empty():
			temp = RL[i].get()
			tempRL[i].put(temp)
			tempRLlate[i].put(temp)
	RL = tempRLlate

	if not RL[2].empty():
		highProcess = RL[2].get()
	elif not RL[1].empty():
		highProcess = RL[1].get()
	else:
		highProcess = RL[0].get()
	RL = tempRL

	if (int(currProcess.priority) < int(highProcess.priority)) or (currProcess.statusType != 'running') or (currProcess.pid == 0):
		currProcess = highProcess
		currProcess.statusType = 'running'
	print(currProcess.pid, end=' ', flush=True)

def pidToIndex(pid):
	for p in pcbList:
		if p.pid == pid:
			return p.pcbIndex

def killTree(i):
	if pcbList[i].child != -1:
		curDel = pcbList[i].child
		while curDel != -1:
			nextDel = pcbList[curDel].younger 
			killTree(curDel)
			curDel = nextDel
	pcbList[i].pid = 0
	if pcbList[i].older == -1:
		pcbList[pcbList[i].parent].child = pcbList[i].younger
		pcbList[pcbList[i].younger].older = -1
		for r in range(1,5):
			if not pcbList[i].rcb[r][1] == 0:
				irelease(i, r, pcbList[i].rcb[r][1])
	elif i == pcbList[i].toDelete:
		pcbList[pcbList[i].older].younger = pcbList[i].younger
		for r in range(1,5):
			if not pcbList[i].rcb[r][1] == 0:
				irelease(i, r, pcbList[i].rcb[r][1])

def request(rid, num):
	global currProcess
	global rcbList
	rid = int(rid)
	num = int(num)

	if rcbList[rid].free >= num:
		rcbList[rid].free = rcbList[rid].free - num
		currProcess.rcb[rid][1] = currProcess.rcb[rid][1] + num
	else:
		currProcess.statusType = 'blocked'
		currProcess.statusList = rcbList
		rcbList[rid].wait.put([RL[int(currProcess.priority)].get(), num])
		
def irelease(pcbI, rid, num):
	global currProcess
	global pcbList
	global rcbList
	rid = int(rid)
	num = int(num)
	rcbList[rid].free = rcbList[rid].free + num
	pcbList[pcbI].rcb[rid][1] = pcbList[pcbI].rcb[rid][1] - num
	#deep copy
	temprcbList1 = queue.Queue()
	while not rcbList[rid].wait.empty():
		temp = rcbList[rid].wait.get()
		if not temp[0].pid == 0:
			temprcbList1.put(temp)
	rcbList[rid].wait = temprcbList1
	#deep copy
	if not rcbList[rid].wait.empty():
		#deep copy
		temprcbList = queue.Queue()
		temprcbListlate = queue.Queue()
		while not rcbList[rid].wait.empty():
			temp = rcbList[rid].wait.get()
			temprcbList.put(temp)
			temprcbListlate.put(temp)
		rcbList[rid].wait = temprcbListlate
		#deep copy
		nextOnWait = rcbList[rid].wait.get()
		temp = nextOnWait[0]
		while nextOnWait[1] <= rcbList[rid].free:
			temp.statusType = 'ready'
			RL[int(temp.priority)].put(temp)
			rcbList[rid].free = rcbList[rid].free - nextOnWait[1]
			temp.rcb[rid][1] = nextOnWait[1]

			if rcbList[rid].wait.empty():
				break

			#deep copy
			temprcbList = queue.Queue()
			temprcbListlate = queue.Queue()
			while not rcbList[rid].wait.empty():
				teemp = rcbList[rid].wait.get()
				temprcbList.put(teemp)
				temprcbListlate.put(teemp)
			rcbList[rid].wait = temprcbListlate
			#deep copy
			
			nextOnWait = rcbList[rid].wait.get()
			temp = nextOnWait[0]

		rcbList[rid].wait = temprcbList

def release(rid, num):
	global currProcess
	global pcbList
	global rcbList
	rid = int(rid)
	num = int(num)
	rcbList[rid].free = rcbList[rid].free + num
	currProcess.rcb[rid][1] = currProcess.rcb[rid][1] - num
	#deep copy
	temprcbList1 = queue.Queue()
	while not rcbList[rid].wait.empty():
		temp = rcbList[rid].wait.get()
		if not temp[0].pid == 0:
			temprcbList1.put(temp)
	rcbList[rid].wait = temprcbList1
	#deep copy
	if not rcbList[rid].wait.empty():
		#deep copy
		temprcbList = queue.Queue()
		temprcbListlate = queue.Queue()
		while not rcbList[rid].wait.empty():
			temp = rcbList[rid].wait.get()
			temprcbList.put(temp)
			temprcbListlate.put(temp)
		rcbList[rid].wait = temprcbListlate
		#deep copy
		nextOnWait = rcbList[rid].wait.get()
		temp = nextOnWait[0]
		while nextOnWait[1] <= rcbList[rid].free:
			temp.statusType = 'ready'
			RL[int(temp.priority)].put(temp)
			rcbList[rid].free = rcbList[rid].free - nextOnWait[1]
			temp.rcb[rid][1] = nextOnWait[1]

			if rcbList[rid].wait.empty():
				break

			#deep copy
			temprcbList = queue.Queue()
			temprcbListlate = queue.Queue()
			while not rcbList[rid].wait.empty():
				teemp = rcbList[rid].wait.get()
				temprcbList.put(teemp)
				temprcbListlate.put(teemp)
			rcbList[rid].wait = temprcbListlate
			#deep copy
			
			nextOnWait = rcbList[rid].wait.get()
			temp = nextOnWait[0]

		rcbList[rid].wait = temprcbList

def canDelete(i, tode):
	can = False
	if i == tode:
		return True
	else:
		if pcbList[i].child != -1:
			curDel = pcbList[i].child
			while curDel != -1:
				nextDel = pcbList[curDel].younger 
				can = can or canDelete(curDel, tode)
				curDel = nextDel
	return can
	



#############COMMAND HANDLING#############
def initCmd(cmd):
	global RL
	global BL
	global pcbList
	global currProcess
	global rcbList
	RL = [queue.Queue(), queue.Queue(), queue.Queue()]
	BL = 0
	pcbList = []
	currProcess = 0
	rcbList = [0, rcb(1), rcb(2), rcb(3), rcb(4)]

	initProcess()
	print('init ', end='', flush=True)
	
def crCmd(cmd):
	global currProcess
	global RL
	global rcbList
	
	for i in range(len(pcbList)):
		if pcbList[i].pid == cmd[1]:
			print("error", end='', flush=True)
			return

	if (cmd[2] < '1' or cmd[2] > '2'):
		print("error", end='', flush=True)
		return
	newProcess = pcb(cmd[1], cmd[2])
	newProcess.parent = currProcess.pcbIndex

	if currProcess.child == -1:
		currProcess.child = newProcess.pcbIndex
	else:
		sibID = currProcess.child
		while pcbList[sibID].younger != -1:
			sibID = pcbList[sibID].younger
		pcbList[sibID].younger = newProcess.pcbIndex
		newProcess.older = sibID

	RL[int(newProcess.priority)].put(newProcess)
	pcbList.append(newProcess)
	scheduler()


def deCmd(cmd):
	global currProcess
	if not canDelete(currProcess.pcbIndex, pidToIndex(cmd[1])) or cmd[1] == 'init':
		print("error", end='', flush=True)
		return
	pcbList[pidToIndex(cmd[1])].toDelete = pidToIndex(cmd[1])
	killTree(pidToIndex(cmd[1]))
	scheduler()

def reqCmd(cmd):
	global currProcess
	if (int( cmd[1][1]) > 4 or cmd[2] == '0' or currProcess.pid == 'init' or int(cmd[1][1]) < (currProcess.rcb[int(cmd[1][1])][1] + int(cmd[2]))):
		print("error", end='', flush=True)
		return
	request(int(cmd[1][1]), cmd[2])
	scheduler()
		

def relCmd(cmd):
	if (int (cmd[1][1]) > 4 or cmd[2] == '0' or currProcess.pid == 'init' or currProcess.rcb[int(cmd[1][1])][1] < int(cmd[2])):
		print("error", end='', flush=True)
		return
	release(int(cmd[1][1]), cmd[2])
	scheduler()

def toCmd(cmd):
	global currProcess
	global RL

	RL[int(currProcess.priority)].get()
	currProcess.statusType = 'ready'
	RL[int(currProcess.priority)].put(currProcess)
	scheduler()
		

def null(cmd):
	print(flush=True);

#############DEBUG FUNCTIONS#############
def p(cmd):
	cmd[1] = int(cmd[1])
	print("pid:", pcbList[cmd[1]].pid) 
	print("priority:", pcbList[cmd[1]].priority) 
	print("status:", pcbList[cmd[1]].statusType) 
	print("sList:", pcbList[cmd[1]].statusList) 
	print("parent:", pcbList[cmd[1]].parent) 
	print("older:", pcbList[cmd[1]].older) 
	
	print("rcb:", pcbList[cmd[1]].rcb) 
	print("child:", pcbList[cmd[1]].child) 
	print("younger:", pcbList[cmd[1]].younger) 

	print(RL)
	print(pcbList)


options = {
	'init': initCmd,
	'cr': crCmd,
	'de': deCmd,
	'req': reqCmd,
	'rel': relCmd,
	'to': toCmd,
	'': null,
	'p': p,
}

#############INITIALIZATION#############
def handleCmd():
	for line in stdin:
		line = line[0: len(line)-1]
		line = line.rstrip('\r')
		cmd = line.split(' ')
		options[cmd[0]](cmd)

def initProcess():
	global currProcess
	global RL
	init = pcb("init", 0)
	RL[0].put(init)
	pcbList.append(init)
	currProcess = init
	
if __name__ == '__main__':
	initCmd(0)
	handleCmd()
