from sys import *
import sys

PM = [0] * 524288
BM = [0] * 1024
BM[0] = 1
TLB = [(0,-1, 0), (1, -1, 0), (2, -1, 0), (3, -1, 0)]

def addPT(s, f):
	global PM
	global BM

	PM[s] = f
	if not f == -1:
		BM[int(f/512)] = 1
		BM[int(f/512)+1] = 1

def addPage(p, s, f):
	global PM
	global BM

	PM[PM[s] + p] = f
	BM[int(f/512)] = 1

def findPage(s, p, w):
	global TLB
	if len(sys.argv) == 3:
		print('m', end=' ')
	if PM[s] == 0 or PM[s] == -1:
		return PM[s] 
	if PM[PM[s] + p] == 0 or PM[PM[s] + p] == -1:
		return PM[PM[s] + p]
	for i in range(len(TLB)):
		TLB[i] = (TLB[i][0] - 1, TLB[i][1], TLB[i][2])
	for i in range(len(TLB)):
		if TLB[i][0] == -1:
			TLB[i] = (3, str(s) + str(p), PM[PM[s] + p])
			break
	return PM[PM[s] + p] + w

def initPM():
	file1 = open(sys.argv[1], "r")
	line1 = file1.readline()
	line2 = file1.readline()
	line1 = line1.rstrip('\n')
	line1 = line1.rstrip('\r')
	line2 = line2.rstrip('\n')
	line2 = line2.rstrip('\r')
	sline1 = line1.split(" ")
	for i in range(0, int(len(sline1)),2):
		s = int(sline1[i])
		f = int(sline1[i+1])
		addPT(s, f)

	sline2 = line2.split(" ")
	for i in range(0, int(len(sline2)),3):
		p = int(sline2[i])
		s = int(sline2[i+1])
		f = int(sline2[i+2])
		addPage(p, s, f)

def findBlank(size):
	global BM
	if size == 0:
		for i in range(len(BM)):
			if BM[i] == 0:
				return i
	if size == 1:
		for i in range(len(BM)):
			if BM[i+1] == 0 and BM[i] == 0:
				return i

def handlePA(rw, s):
	if rw == 0:
		if s == 0:
			return 'err'
		if s == -1:
			return 'pf'
		return s
	else:
		if s == -1:
			return "pf"
		if s != 0:
			return s
		return "should never get here"
			

def convertVA(rw, VA):
	global PM
	global BM
	global TLB
	spw = "00000000000000000000000000000000"
	spw = spw + bin(VA)[2:]
	spw = spw[len(spw)-32:]
	s = int(spw[4:13], 2)
	p = int(spw[13:23], 2)
	w = int(spw[23:32], 2)
	if rw == 1:
		if PM[s] == 0:
			addPT(s, findBlank(1) * 512)
		if PM[PM[s] + p] == 0 and not PM[s] == -1:
			newpage = int(findBlank(0))
			PM[PM[s] + p] = newpage * 512
			BM[newpage] = 1
	for i in range(len(TLB)):
		if TLB[i][1] == str(s) + str(p):
			if len(sys.argv) == 3:
				print("h", end=' ')
			for j in range(len(TLB)):
				if TLB[j][0] > TLB[i][0]:
					TLB[j] = (TLB[j][0] - 1, TLB[j][1], TLB[j][2])
			TLB[i] = (3, TLB[i][1], TLB[i][2])
			return handlePA(rw, TLB[i][2]+w)
	return handlePA(rw, findPage(s, p, w))

def handleCmd():
	file2 = open(sys.argv[2], "r")
	line = file2.readline()
	sline = line.split(" ")
	for i in range(0, int(len(sline)),2):
		print(convertVA(int(sline[i]), int(sline[i+1])), end=' ')

if __name__ == '__main__':
	initPM()
	handleCmd()
