from sys import *
import math
import queue

class process():
	def __init__(self, a, t):
		self.arrive = int(a)
		self.time = int(t)
		self.end = 0
		self.total = 0
		self.sjfAdded = False
		self.srtAdded = False
		self.priority = -1
		self.timeInPriority = 0


def printResult(plist, times):
	avg = str.format('{0:.2f}', (float(sum(times) / max(len(times), 1))))
	print(avg, end=' ')
	for p in plist:
		print(p.total, end=' ')
	print()

def runFifo(plist):
	totalTime = 0
	times = []
	pArrived = []

	while not len(times) == len(plist):
		for p in plist:
			if p.arrive <= totalTime and p.end == 0:
				pArrived.append(p)

		if len(pArrived) == 0:
			totalTime = totalTime + 1
		else:
			totalTime = pArrived[0].time + totalTime
			pArrived[0].end = totalTime
			pArrived[0].total = totalTime - pArrived[0].arrive
			times.append(pArrived[0].total)
			pArrived = []

	printResult(plist, times)
	'''
	totalTime = 0
	times = []
	for p in plist:
		totalTime = totalTime + p.time
		p.end = totalTime
		p.total = p.end - p.arrive
		times.append(p.total)
	printResult(plist, times)
	'''

def runSjf(plist):
	totalTime = 0
	times = []
	pArrived = []

	while not len(times) == len(plist):
		for p in plist:
			if p.arrive <= totalTime and p.end == 0:
				pArrived.append(p)

		if len(pArrived) == 0:
			totalTime = totalTime + 1
		else:
			minp = pArrived[0]
			for pa in pArrived:
				if pa.time < minp.time:
					minp = pa

			totalTime = totalTime + minp.time
			minp.end = totalTime
			minp.total = minp.end - minp.arrive
			times.append(minp.total)
			pArrived = []
	
	printResult(plist, times)

def runSrt(plist):
	totalTime = 0
	times = []
	pArrived = []

	while not 0 == (sum(p.time for p in plist)):
		for p in plist:
			if p.arrive <= totalTime and not p.time == 0:
				pArrived.append(p)

		if len(pArrived) == 0:
			totalTime = totalTime + 1
		else:
			minp = pArrived[0]
			for pa in pArrived:
				if pa.time < minp.time:
					minp = pa
			
			totalTime = totalTime + 1
			minp.time = minp.time - 1
			if minp.time == 0:
				minp.end = totalTime
				minp.total = minp.end - minp.arrive
				times.append(minp.total)
			pArrived = []
	
	printResult(plist, times)
	

def runMlf(plist):
	mlist = [[], [], [], [], []]
	totalTime = 0
	times = []

	while not (sum(p.time for p in plist)) == 0:
		for p in plist:
			if p.arrive <= totalTime and p.priority == -1:
				p.priority = p.priority + 1
				mlist[p.priority].append(p)

		toDelete = 0
		for pri in mlist:
			if not len(pri) == 0:
				for p in pri:
					if not p.time == 0:
						if p.timeInPriority == math.pow(2, p.priority)-1 and not p.priority == 4:
							p.priority = p.priority + 1
							p.timeInPriority = 0
							mlist[p.priority].append(p)
							toDelete = p
							p.time = p.time - 1
							totalTime = totalTime + 1
							if p.time == 0:
								p.end = totalTime
								p.total = p.end - p.arrive
								times.append(p.total)
							break
						else:
							p.timeInPriority = p.timeInPriority + 1
							p.time = p.time - 1
							totalTime = totalTime + 1
							toDelete = 1
							if p.time == 0:
								p.end = totalTime
								p.total = p.end - p.arrive
								times.append(p.total)
							break
				if not toDelete == 0:
					break

		if toDelete == 1:
			toDelete = 0
		elif not toDelete == 0:
			mlist[toDelete.priority-1].remove(toDelete)
		else:
			totalTime = totalTime + 1


	printResult(plist, times)


#############INITIALIZATION#############
def createProcesses():
	plist = []
	for line in stdin:
		line = line.rstrip('\r')
		line = line.rstrip('\n')
		sline = line.split(' ')
		for i in range(0, int(len(sline)),2):
			p = process(sline[i], sline[i+1])
			plist.append(p)
	return plist

if __name__ == '__main__':
	plist = createProcesses()
	plist2 = []
	plist3 = []
	plist4 = []
	for p in plist:
		plist2.append(process(p.arrive, p.time))
		plist3.append(process(p.arrive, p.time))
		plist4.append(process(p.arrive, p.time))
	runFifo(plist)
	runSjf(plist2)
	runSrt(plist3)
	runMlf(plist4)
