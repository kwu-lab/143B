if ! [ -a $1 ]
then
	echo NO FILE
	exit 1
fi

failCount=0
testCases="$(ls test/*.txt | wc -l)"
declare -A failList 

case "$2" in

	update-expected )
		echo Updating $1...
		for f in ./test/*.txt; do
			if [ -a $f.out ]
			then
				cp $f.out $f.expected 
			fi
		done
		echo Updated
		;;


	"" )
		echo Testing $1...
		for f in ./test/*.txt; do
			echo ----------------------
			echo $f:
			python3 $1 < $f > $f.out

			if cmp -s "$f.out" "$f.expected"
			then
				echo SUCCESS
			else
				echo FAIL
				echo actual:
				cat $f.out
				echo
				echo expected:
				if [ -a $f.expected ]
				then
					cat $f.expected
				else
					echo NO FILE
				fi
				echo
				failList[$failCount]=$f
				failCount=$((failCount+1))
			fi
			echo ----------------------
		done
		echo Total Cases : $testCases
		echo Failed Cases: $failCount
		printf '%s\n' "${failList[@]}"
		;;
esac

